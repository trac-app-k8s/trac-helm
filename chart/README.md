# Trac Helm chart

* Installs the web-based management system [Trac](https://trac.edgewall.org)

## Get Repo Info

```plain
helm repo add trac https://gitlab.com/api/v4/projects/39956701/packages/helm/stable
helm repo update
```

_See [helm repo](https://helm.sh/docs/helm/helm_repo/) for command documentation._

## Installing the Chart

To install the chart with the release name `trac-test`:

```plain
helm install trac-test trac/trac
```

## Uninstalling the Chart

To uninstall/delete the `trac-test` deployment:

```console
helm delete trac-test
```

The command removes all the Kubernetes components associated with the chart and deletes the release.

## Configuration

| Parameter                                    | Description                                          | Default                                                                              |
|----------------------------------------------|------------------------------------------------------|--------------------------------------------------------------------------------------|
| `replicaCount`                               | Number of podes                                      | `1`                                                                                  |
| `image.repository`                           | Image repository                                     | `registry.gitlab.com/trac-app-k8s/trac-docker`                                       |
| `image.tag`                                  | Overrides the Trac image tag                         | `main`                                                                               |
| `image.pullPolicy`                           | Image pull policy                                    | `IfNotPresent`                                                                       |
| `imagePullSecrets`                           | Image pull secrets                                   | `[]`                                                                                 |
| `nameOverride`                               | Image pull secrets                                   | `""`                                                                                 |
| `fullnameOverride`                           | Image pull secrets                                   | `""`                                                                                 |
| `env`                                        | Extra environment variables passed to pods           | `[]`                                                                                 |
| `customConfig`                               | Extra configuration passed to application            | `""`                                                                                 |
| `postgresql`                                 | Extra configuration passed to bundled postgres chart |                                                                                      |
| `serviceAccount.create`                      | Create service account                               | `true`                                                                               |
| `serviceAccount.annotations`                 | ServiceAccount annotations                           | `{}`                                                                                 |
| `serviceAccount.name`                        | Service account name to use                          | `""`                                                                                 |
| `podAnnotations`                             | Pod annotations                                      | `{}`                                                                                 |
| `podSecurityContext`                         | Pod security context                                 | `{}`                                                                                 |
| `securityContext`                            | Security context                                     | `{}`                                                                                 |
| `service.type`                               | Kubernetes service type                              | `ClusterIP`                                                                          |
| `service.port`                               | Kubernetes port where service is exposed             | `8000`                                                                               |
| `ingress.enabled`                            | Enables Ingress                                      | `true`                                                                               |
| `ingress.ingressClassName`                   | Ingress class name                                   | `""`                                                                                 |
| `ingress.annotations`                        | Ingress annotations                                  | `{}`                                                                                 |
| `ingress.hosts`                              | Ingress accepted hostnames                           | `[{"host":"trac.local","paths":[{"path":"/","pathType":"ImplementationSpecific"}]}]` |
| `ingress.tls`                                | Ingress TLS configuration                            | `[]`                                                                                 |
| `resources`                                  | CPU/Memory resource requests/limits                  | `{}`                                                                                 |
| `autoscaling.enabled`                        | Enables HPA                                          | `false`                                                                              |
| `autoscaling.minReplicas`                    | Minimal replicas count                               | `1`                                                                                  |
| `autoscaling.maxReplicas`                    | Maximum replicas count                               | `100`                                                                                |
| `autoscaling.targetCPUUtilizationPercentage` | CPU utilization percent to trigger scale up          | `80`                                                                                 |
| `nodeSelector`                               | Node labels for pod assignment                       | `{}`                                                                                 |
| `tolerations`                                | Toleration labels for pod assignment                 | `[]`                                                                                 |
| `affinity`                                   | Affinity settings for pod assignment                 | `{}`                                                                                 |
| `secrets.TRAC_ADMIN_PASSWORD`                | Secret environment variables for application         | `""`                                                                                 |

## Using `customConfig`

You can write content of `trac.ini` to `customConfig` like so:

```yaml
customConfig: |
  [changeset]
  max_diff_bytes = 10000000
  max_diff_files = 0
  wiki_format_messages = enabled

  [header_logo]
  alt = (please configure the [header_logo] section in trac.ini)
  height = -1
  link =
  src = site/your_project_logo.png
  width = -1
```

When specified `env` section ignored completely
