# Trac Helm chart

Builded chart packages stored in [Packages](https://gitlab.com/trac-app-k8s/trac-helm/-/packages).

Values reference and install instructions can be found in [chart](/chart) directory.
